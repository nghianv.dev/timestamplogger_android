package tools.timestamplogger.ui.activites;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import tools.timestamplogger.R;
import tools.timestamplogger.common.bus.BusProvider;
import tools.timestamplogger.common.bus.objects.NewSyncData;
import tools.timestamplogger.common.local_data.ListOverallEvaluationPendingSyncLocal;
import tools.timestamplogger.common.local_data.ListUserPendingSyncLocal;
import tools.timestamplogger.common.local_data.UserLocal;
import tools.timestamplogger.common.objects.ListOverallEvaluationPendingSync;
import tools.timestamplogger.common.objects.User;

/**
 * Created by nghianv on 5/16/17
 */
@SuppressLint("Registered")
@EActivity(R.layout.activity_overall_evaluation)
public class OverallEvaluationActivity extends AppCompatActivity {

    @ViewById(R.id.spinnerRideComfort)
    Spinner spinnerRideComfort;
    @ViewById(R.id.spinnerRoadCondition)
    Spinner spinnerRoadCondition;
    @ViewById(R.id.spinnerDrivingCondition)
    Spinner spinnerDrivingCondition;
    @ViewById(R.id.spinnerVehicleCondition)
    Spinner spinnerVehicleCondition;

    @Click(R.id.btnAction)
    void onFinish() {
        User user = UserLocal.getUser();
        assert user != null;
        ListOverallEvaluationPendingSyncLocal.saveOverallEvaluation(ListOverallEvaluationPendingSync.builder()
                .userId(user.get_id())
                .rideComfort(Integer.parseInt(spinnerRideComfort.getSelectedItem().toString().trim()))
                .roadCondition(Integer.parseInt(spinnerRoadCondition.getSelectedItem().toString().trim()))
                .drivingCondition(Integer.parseInt(spinnerDrivingCondition.getSelectedItem().toString().trim()))
                .vehicleCondition(Integer.parseInt(spinnerVehicleCondition.getSelectedItem().toString().trim()))
                .build());

        ListUserPendingSyncLocal.updateReadyStatus(user.get_id(), 1);
        BusProvider.getInstance().post(NewSyncData.builder().build());
        Intent intent = new Intent();
        intent.putExtra("IS_SAVE", true);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @AfterViews
    void afterViews() {
        initSpinners();
    }

    private void initSpinners() {
        List<String> categories = new ArrayList<>();
        categories.add("1");
        categories.add("2");
        categories.add("3");
        categories.add("4");
        categories.add("5");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerRideComfort.setAdapter(dataAdapter);
        spinnerRoadCondition.setAdapter(dataAdapter);
        spinnerDrivingCondition.setAdapter(dataAdapter);
        spinnerVehicleCondition.setAdapter(dataAdapter);
    }
}

package tools.timestamplogger.ui.activites;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import tools.timestamplogger.R;
import tools.timestamplogger.services.DataSync;

/**
 * Created by nghianv on 5/18/17
 */
@SuppressLint("Registered")
@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity {

    @AfterViews
    void afterViews() {
        if (!DataSync.isRunning()) {
            startService(new Intent(this, DataSync.class));
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                MainActivity_.intent(SplashActivity.this).start();
                finish();
            }
        }, 3000);
    }
}

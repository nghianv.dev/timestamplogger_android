package tools.timestamplogger.ui.activites;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import tools.timestamplogger.R;
import tools.timestamplogger.common.local_data.ListFeelPendingSyncLocal;
import tools.timestamplogger.common.local_data.ListOverallEvaluationPendingSyncLocal;
import tools.timestamplogger.common.local_data.ListUserPendingSyncLocal;
import tools.timestamplogger.common.local_data.UserLocal;
import tools.timestamplogger.common.objects.ListFeelPendingSync;
import tools.timestamplogger.common.objects.ListUserPendingSync;
import tools.timestamplogger.common.objects.User;
import tools.timestamplogger.ui.dialogs.AgeDialog;
import tools.timestamplogger.ui.dialogs.GenderDialog;
import tools.timestamplogger.ui.dialogs.HeightDialog;
import tools.timestamplogger.ui.dialogs.PositionDialog;
import tools.timestamplogger.ui.dialogs.WeightDialog;

@SuppressLint("Registered")
@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity
        implements GenderDialog.GenderListening, AgeDialog.AgeListening, WeightDialog.WeightListening,
        HeightDialog.HeightListening, PositionDialog.PositionListening {

    private final int SAVE_CODE = 1;
    private int FEEL = 0;

    private MediaPlayer mediaPlayerFeelPress;
    private MediaPlayer mediaPlayerFeelStart;

    private boolean isPendingGpsStart = false;
    private boolean isCapture = false;
    private boolean isRunning = false;

    private String participant = "";
    private String gender = "";
    private String position = "";
    private int age = 0;
    private int weight = 0;
    private int height = 0;
    private int busRoute = 0;

    @ViewById(R.id.tvTimer)
    TextView tvTimer;
    @ViewById(R.id.imbFeel1)
    TextView imbFeel1;
    @ViewById(R.id.imbFeel2)
    TextView imbFeel2;
    @ViewById(R.id.imbFeel3)
    TextView imbFeel3;
    @ViewById(R.id.imbFeel4)
    TextView imbFeel4;
    @ViewById(R.id.imbFeel5)
    TextView imbFeel5;
    @ViewById(R.id.tvGender)
    TextView tvGender;
    @ViewById(R.id.tvAge)
    TextView tvAge;
    @ViewById(R.id.tvWeight)
    TextView tvWeight;
    @ViewById(R.id.tvHeight)
    TextView tvHeight;
    @ViewById(R.id.edtParticipant)
    EditText edtParticipant;
    @ViewById(R.id.edtBusRoute)
    EditText edtBusRoute;
    @ViewById(R.id.tvPosition)
    TextView tvPosition;
    @ViewById(R.id.tvPositionTitle)
    TextView tvPositionTitle;
    @ViewById(R.id.btnAction)
    Button btnAction;
    @ViewById(R.id.btnClear)
    Button btnClear;
    @ViewById(R.id.lnGender)
    LinearLayout lnGender;
    @ViewById(R.id.lnAge)
    LinearLayout lnAge;
    @ViewById(R.id.lnWeight)
    LinearLayout lnWeight;
    @ViewById(R.id.lnHeight)
    LinearLayout lnHeight;


    private static final int LOCATION_REQUEST_CODE = 1;
    private LocationManager mLocationManager;
    private User user;

    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            isPendingGpsStart = true;
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    @SuppressLint("SetTextI18n")
    private void emptyInfo() {
        edtParticipant.setText("");
        tvGender.setText("_");
        tvAge.setText("_ years old");
        tvWeight.setText("_ (Kg)");
        tvHeight.setText("_ (cm)");
        edtBusRoute.setText("");
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void fillInfo() {
        edtParticipant.setText(participant);
        tvGender.setText(gender);
        tvAge.setText(String.format("%d years old", age));
        tvWeight.setText(String.format("%d (Kg)", weight));
        tvHeight.setText(String.format("%d (cm)", height));
        edtBusRoute.setText(String.valueOf(busRoute));
        tvPosition.setText(position);
    }


    @Click(R.id.btnClear)
    void onClearPress() {
        if (isRunning) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle("Warning! app is running")
                    .setMessage("Are you sure you want to delete this review?")
                    .setIcon(android.R.drawable.stat_sys_warning)
                    .setCancelable(false)
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            isRunning = false;
                            isCapture = false;
                            emptyInfo();

                            ListFeelPendingSyncLocal.deleteFeelsSyncUser(user.get_id());
                            ListOverallEvaluationPendingSyncLocal.deleteOverallEvaluationUser(user.get_id());
                            ListUserPendingSyncLocal.deleteUserPendingSync(user.get_id());
                            user = null;

                            dialog.dismiss();
                            stopView();
                        }
                    }).setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).show();
        } else {
            emptyInfo();
            UserLocal.deleteUser();
        }
    }


    @Click(R.id.lnGender)
    void onOpenGender() {
        GenderDialog.open(getSupportFragmentManager(), tvGender.getText().toString().trim().toLowerCase());
    }

    @Click(R.id.lnAge)
    void onOpenAge() {
        String ageStr = tvAge.getText().toString().trim().replace("years old", "").trim();
        int age = 0;
        if (!ageStr.equals("_")) {
            age = Integer.parseInt(ageStr);
        }
        AgeDialog.open(getSupportFragmentManager(), age);
    }

    @Click(R.id.lnWeight)
    void onOpenWeight() {
        String weightStr = tvWeight.getText().toString().trim().replace("(Kg)", "").trim();
        int weight = 0;
        if (!weightStr.equals("_")) {
            weight = Integer.parseInt(weightStr);
        }
        WeightDialog.open(getSupportFragmentManager(), weight);
    }

    @Click(R.id.lnHeight)
    void onOpenHeight() {
        String heightStr = tvHeight.getText().toString().trim().replace("(cm)", "").trim();
        int height = 0;
        if (!heightStr.equals("_")) {
            height = Integer.parseInt(heightStr);
        }
        HeightDialog.open(getSupportFragmentManager(), height);
    }

    @Click({R.id.tvPosition, R.id.tvPositionTitle})
    void onOpenPosition() {
        PositionDialog.open(getSupportFragmentManager(), tvPosition.getText().toString().trim().toLowerCase());
    }

    @SuppressLint("SetTextI18n")
    @Click(R.id.btnAction)
    void onAction() {
        String action = btnAction.getText().toString().trim().toLowerCase();
        switch (action) {
            case "start":
                participant = edtParticipant.getText().toString().trim();
                if (participant.length() == 0) {
                    showDialog("Information is invalid", "Please enter your name in `participant box`", new DialogListening() {
                        @Override
                        public void onOk() {
                            edtParticipant.setText("");
                            edtParticipant.requestFocus();
                        }
                    });
                    return;
                }

                if (position.length() == 0) {
                    showDialog("Information is invalid", "Please enter invalid `position`", new DialogListening() {
                        @Override
                        public void onOk() {
                            onOpenPosition();
                        }
                    });
                    return;
                }


                String busRouteStr = edtBusRoute.getText().toString().trim();
                if (busRouteStr.length() == 0 || busRouteStr.length() > 7) {
                    showDialog("Information is invalid", "Please enter invalid `Bus route`", new DialogListening() {
                        @Override
                        public void onOk() {
                            edtBusRoute.setText("");
                            edtBusRoute.requestFocus();
                        }
                    });
                    return;
                }
                busRoute = Integer.parseInt(busRouteStr);

                if (gender.equals("")) {
                    showDialog("Information is invalid", "Please choose your gender", new DialogListening() {
                        @Override
                        public void onOk() {
                            onOpenGender();
                        }
                    });
                    return;
                }

                if (age == 0) {
                    showDialog("Information is invalid", "Please enter your age", new DialogListening() {
                        @Override
                        public void onOk() {
                            onOpenAge();
                        }
                    });
                    return;
                }

                if (weight == 0) {
                    showDialog("Information is invalid", "Please enter your weight", new DialogListening() {
                        @Override
                        public void onOk() {
                            onOpenWeight();
                        }
                    });
                    return;
                }

                if (height == 0) {
                    showDialog("Information is invalid", "Please enter your height", new DialogListening() {
                        @Override
                        public void onOk() {
                            onOpenWeight();
                        }
                    });
                    return;
                }

//                if (!isPendingGpsStart) {
//                    showDialog("Application is not ready", "Wait for the gps to boot up. Try again in a few seconds", new DialogListening() {
//                        @Override
//                        public void onOk() {
//
//                        }
//                    });
//                    return;
//                }


                isRunning = true;
                user = User.builder()._id(String.valueOf(new Date().getTime())).participant(participant).busRoute(busRoute).gender(gender).position(position)
                        .age(age).weight(weight).height(height).isStart(1).build();
                UserLocal.saveUser(user);
                ListUserPendingSyncLocal.addUserPendingSync(ListUserPendingSync.builder()
                        .userId(user.get_id())
                        .participant(user.getParticipant())
                        .gender(user.getGender())
                        .age(user.getAge())
                        .weight(user.getWeight())
                        .height(user.getHeight())
                        .busRoute(user.getBusRoute())
                        .position(position)
                        .ready(0)
                        .build());
                mediaPlayerFeelStart.start();
                disableViewsInfo();
                enableViewsFeel();
                btnAction.setText("Stop");
                btnAction.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.custom_selector_stop));
                break;
            case "stop":
                if (ListFeelPendingSyncLocal.getFeels(user.get_id()).size() == 0) {
                    showDialog("No data", "You have no experience during the experience. If you want to exit, press `C` button", new DialogListening() {
                        @Override
                        public void onOk() {

                        }
                    });
                } else {
                    OverallEvaluationActivity_.intent(this).startForResult(SAVE_CODE);
                }
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    private void stopView() {
        btnAction.setText("Start");
        btnAction.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.custom_selector_start));
        disableViewsFeel();
        enableViewsInfo();
    }

    @Click(R.id.imbFeel1)
    void onFeel1Press() {
        feelSubmit(1);
    }

    @Click(R.id.imbFeel2)
    void onFeel2Press() {
        feelSubmit(2);
    }

    @Click(R.id.imbFeel3)
    void onFeel3Press() {
        feelSubmit(3);
    }

    @Click(R.id.imbFeel4)
    void onFeel4Press() {
        feelSubmit(4);
    }

    @Click(R.id.imbFeel5)
    void onFeel5Press() {
        feelSubmit(5);
    }

    private void feelSubmit(int feel) {
        isCapture = true;
        mediaPlayerFeelPress.start();
        disableViewsFeel();
        this.FEEL = feel;
        Criteria criteria = new Criteria();
        String bestProvider = mLocationManager.getBestProvider(criteria, false);
        @SuppressLint("MissingPermission")
        Location location = mLocationManager.getLastKnownLocation(bestProvider);
        hasLocation(location);
    }

    @SuppressLint("SetTextI18n")
    @AfterViews
    void onAfterViews() {
        initMediaPlayers();
        initLocation();
        updateAppTimer();

        // kiem tra xem dang chay khong.
        if (UserLocal.getUser() != null) {
            user = UserLocal.getUser();
            assert user != null;
            participant = user.getParticipant();
            gender = user.getGender();
            age = user.getAge();
            weight = user.getWeight();
            height = user.getHeight();
            busRoute = user.getBusRoute();
            position = user.getPosition();
            fillInfo();

            if (user.getIsStart() == 1) {
                isRunning = true;
                disableViewsInfo();
                enableViewsFeel();
                btnAction.setText("Stop");
                btnAction.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.custom_selector_stop));
            }
        } else {
            enableViewsInfo();
            emptyInfo();
            disableViewsFeel();
        }
    }

    private void initMediaPlayers() {
        mediaPlayerFeelPress = MediaPlayer.create(this, R.raw.raw_sound_2);
        mediaPlayerFeelPress.setVolume(0.5f, 0.5f);
        mediaPlayerFeelStart = MediaPlayer.create(this, R.raw.raw_sound_3);
    }

    private void initLocation() {
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        getCurrentPosition();
    }

    private void updateAppTimer() {
        final Handler handler = new Handler();
        Runnable updateTimerThread = new Runnable() {
            public void run() {
                Calendar c = Calendar.getInstance();
                @SuppressLint("SimpleDateFormat") final SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
                String formattedDate = df.format(c.getTime());
                tvTimer.setText(formattedDate);
                handler.postDelayed(this, 1000);
            }
        };
        handler.postDelayed(updateTimerThread, 0);
    }

    private void hasLocation(Location location) {
        if (isCapture) {
            Calendar c = Calendar.getInstance();
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss a - dd/MM/yyyy");
            String formattedDate = df.format(c.getTime());

            List<String> dmsLocation = convertLocationToDMSFormat(location.getLatitude(), location.getLongitude());

            ListFeelPendingSyncLocal.saveFeel(ListFeelPendingSync.builder()
                    .userId(user.get_id())
                    .time(formattedDate)
                    .timestamp(new Date().getTime())
                    .lat(dmsLocation.get(0))
                    .lng(dmsLocation.get(1))
                    .feel(FEEL)
                    .build());

            isCapture = false;
            enableViewsFeel();
        }
    }

    private List<String> convertLocationToDMSFormat(double latitude, double longitude) {
        List<String> rsls = new ArrayList<>();

        StringBuilder builder = new StringBuilder();
        String latitudeDegrees = Location.convert(Math.abs(latitude), Location.FORMAT_SECONDS);
        String[] latitudeSplit = latitudeDegrees.split(":");
        builder.append(latitudeSplit[0]);
        builder.append("d ");
        builder.append(latitudeSplit[1]);
        builder.append("m ");
        builder.append(latitudeSplit[2]);
        builder.append("s ");

        if (latitude < 0) {
            builder.append("S");
        } else {
            builder.append("N");
        }
        rsls.add(builder.toString());

        builder = new StringBuilder();

        String longitudeDegrees = Location.convert(Math.abs(longitude), Location.FORMAT_SECONDS);
        String[] longitudeSplit = longitudeDegrees.split(":");
        builder.append(longitudeSplit[0]);
        builder.append("d ");
        builder.append(longitudeSplit[1]);
        builder.append("m ");
        builder.append(longitudeSplit[2]);
        builder.append("s ");
        if (longitude < 0) {
            builder.append("W");
        } else {
            builder.append("E");
        }

        rsls.add(builder.toString());
        return rsls;
    }

    private void getCurrentPosition() {
        int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        if (locationPermission != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            }
        } else {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                            && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
                } else {
                    showDialog("Error permission", "App need get GPS location to run", new DialogListening() {
                        @Override
                        public void onOk() {
                            finish();
                        }
                    });
                }
                break;
        }
    }

    private void showDialog(String title, String message, final DialogListening dialogListening) {
        new AlertDialog.Builder(this).setTitle(title).setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dialogListening.onOk();
                    }
                }).show();
    }

    private void disableViewsInfo() {
        edtParticipant.setEnabled(false);
        lnGender.setEnabled(false);
        lnAge.setEnabled(false);
        lnWeight.setEnabled(false);
        lnHeight.setEnabled(false);
        edtBusRoute.setEnabled(false);
        tvPosition.setEnabled(false);
        tvPositionTitle.setEnabled(false);
    }

    private void enableViewsInfo() {
        edtParticipant.setEnabled(true);
        lnGender.setEnabled(true);
        lnAge.setEnabled(true);
        lnWeight.setEnabled(true);
        lnHeight.setEnabled(true);
        edtBusRoute.setEnabled(true);
        tvPosition.setEnabled(true);
        tvPositionTitle.setEnabled(true);
    }

    private void disableViewsFeel() {
        imbFeel1.setEnabled(false);
        imbFeel2.setEnabled(false);
        imbFeel3.setEnabled(false);
        imbFeel4.setEnabled(false);
        imbFeel5.setEnabled(false);
    }

    private void enableViewsFeel() {
        imbFeel1.setEnabled(true);
        imbFeel2.setEnabled(true);
        imbFeel3.setEnabled(true);
        imbFeel4.setEnabled(true);
        imbFeel5.setEnabled(true);
    }


    @Override
    public void onSelectPosition(String position) {
        this.position = position;
        tvPosition.setText(position);
    }

    @Override
    public void onSelectGender(String gender) {
        this.gender = gender;
        tvGender.setText(gender);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onAgeTyping(int age) {
        this.age = age;
        tvAge.setText(age + " years old");
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onHeightTyping(int height) {
        this.height = height;
        tvHeight.setText(height + " (cm)");
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onWeightTyping(int weight) {
        this.weight = weight;
        tvWeight.setText(weight + " (Kg)");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SAVE_CODE) {
            if (data != null && data.getExtras() != null && data.getExtras().getBoolean("IS_SAVE")) {
                user.setIsStart(0);
                user.save();
                isRunning = false;
                isCapture = false;
                stopView();
            }
        }
    }

    interface DialogListening {
        void onOk();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mediaPlayerFeelPress != null) {
            mediaPlayerFeelPress.release();
            mediaPlayerFeelPress = null;
        }
        if (mediaPlayerFeelStart != null) {
            mediaPlayerFeelStart.release();
            mediaPlayerFeelStart = null;
        }
    }
}

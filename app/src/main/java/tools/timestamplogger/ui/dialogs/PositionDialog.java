package tools.timestamplogger.ui.dialogs;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import tools.timestamplogger.R;
import tools.timestamplogger.ui.intalize.BaseDialog;

/**
 * Created by nghianv on 5/22/17
 */

public class PositionDialog extends BaseDialog {
    private String position = "";

    @Override
    protected int getLayout() {
        return R.layout.dialog_position;
    }

    @Override
    protected void afterViews() {
        intiViews();
    }

    private void intiViews() {
        String oldPosition = getArguments().getString("position");


        final TextView tvOk = (TextView) view.findViewById(R.id.tvOk);
        final RadioButton rdbSeated = (RadioButton) view.findViewById(R.id.rdbSeated);
        final RadioButton rdbStanding = (RadioButton) view.findViewById(R.id.rdbStanding);
        RadioGroup rdgPosition = (RadioGroup) view.findViewById(R.id.rdgPosition);

        if ("seated".equals(oldPosition)) {
            position = "Seated";
            rdbSeated.setChecked(true);
        } else if ("standing".equals(oldPosition)) {
            position = "Standing";
            rdbStanding.setChecked(true);
        }

        rdgPosition.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                position = checkedId == rdbSeated.getId() ? "Seated" : "Standing";
                tvOk.performClick();
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!position.equals("") && getContext() instanceof PositionListening) {
                    ((PositionListening) getContext()).onSelectPosition(position);
                    dismiss();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.AnimBottomTranslateDelay);
        getDialog().getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
    }

    public static void open(FragmentManager fragmentManager, String oldPosition) {
        PositionDialog positionDialog = new PositionDialog();
        Bundle bundle = new Bundle();
        bundle.putString("position", oldPosition);
        positionDialog.setArguments(bundle);
        positionDialog.show(fragmentManager, positionDialog.toString());
    }

    public interface PositionListening {
        void onSelectPosition(String position);
    }
}
package tools.timestamplogger.ui.dialogs;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import tools.timestamplogger.R;
import tools.timestamplogger.ui.intalize.BaseDialog;

/**
 * Created by nghianv on 5/16/17
 */

public class HeightDialog extends BaseDialog {

    @Override
    protected int getLayout() {
        return R.layout.dialog_height;
    }

    @Override
    protected void afterViews() {
        intiViews();
    }

    private void intiViews() {
        int height = getArguments().getInt("height");


        final TextView tvOk = (TextView) view.findViewById(R.id.tvOk);
        final EditText edtHeight = (EditText) view.findViewById(R.id.edtHeight);

        if (height != 0) edtHeight.setText(String.valueOf(height));

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtHeight.getText().toString().trim().length() > 0 && edtHeight.getText().toString().trim().length() < 5 && Integer.parseInt(edtHeight.getText().toString().trim()) > 0 &&
                        getContext() instanceof HeightListening) {
                    ((HeightListening) getContext()).onHeightTyping(Integer.parseInt(edtHeight.getText().toString().trim()));
                    dismiss();
                }
            }
        });

        edtHeight.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                tvOk.performClick();
                return false;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.AnimBottomTranslateDelay);
        getDialog().getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
    }

    public static void open(FragmentManager fragmentManager, int oldHeight) {
        HeightDialog heightDialog = new HeightDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("height", oldHeight);
        heightDialog.setArguments(bundle);
        heightDialog.show(fragmentManager, heightDialog.toString());
    }

    public interface HeightListening {
        void onHeightTyping(int height);
    }
}
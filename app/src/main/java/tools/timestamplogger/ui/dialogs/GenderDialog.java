package tools.timestamplogger.ui.dialogs;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import tools.timestamplogger.R;
import tools.timestamplogger.ui.intalize.BaseDialog;

/**
 * Created by nghianv on 4/18/17
 */

public class GenderDialog extends BaseDialog {
    private String gender = "";

    @Override
    protected int getLayout() {
        return R.layout.dialog_gender;
    }

    @Override
    protected void afterViews() {
        intiViews();
    }

    private void intiViews() {
        String oldGender = getArguments().getString("gender");


        final TextView tvOk = (TextView) view.findViewById(R.id.tvOk);
        final RadioButton rdbMan = (RadioButton) view.findViewById(R.id.rdbMan);
        final RadioButton rdbWoman = (RadioButton) view.findViewById(R.id.rdbWoman);
        RadioGroup rdgGender = (RadioGroup) view.findViewById(R.id.rdgGender);

        if ("female".equals(oldGender)) {
            gender = "Female";
            rdbWoman.setChecked(true);
        } else if ("male".equals(oldGender)) {
            gender = "Male";
            rdbMan.setChecked(true);
        }

        rdgGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                gender = checkedId == rdbMan.getId() ? "Male" : "Female";
                tvOk.performClick();
            }
        });

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!gender.equals("") && getContext() instanceof GenderListening) {
                    ((GenderListening) getContext()).onSelectGender(gender);
                    dismiss();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.AnimBottomTranslateDelay);
        getDialog().getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
    }

    public static void open(FragmentManager fragmentManager, String oldGender) {
        GenderDialog genderDialog = new GenderDialog();
        Bundle bundle = new Bundle();
        bundle.putString("gender", oldGender);
        genderDialog.setArguments(bundle);
        genderDialog.show(fragmentManager, genderDialog.toString());
    }

    public interface GenderListening {
        void onSelectGender(String gender);
    }
}
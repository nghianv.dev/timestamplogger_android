package tools.timestamplogger.ui.dialogs;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import tools.timestamplogger.R;
import tools.timestamplogger.ui.intalize.BaseDialog;

/**
 * Created by nghianv on 5/16/17
 */

public class WeightDialog extends BaseDialog {

    @Override
    protected int getLayout() {
        return R.layout.dialog_weight;
    }

    @Override
    protected void afterViews() {
        intiViews();
    }

    private void intiViews() {
        int weight = getArguments().getInt("weight");


        final TextView tvOk = (TextView) view.findViewById(R.id.tvOk);
        final EditText edtWeight = (EditText) view.findViewById(R.id.edtWeight);

        if (weight != 0) edtWeight.setText(String.valueOf(weight));

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtWeight.getText().toString().trim().length() > 0 && edtWeight.getText().toString().trim().length() < 5 && Integer.parseInt(edtWeight.getText().toString().trim()) > 0 &&
                        getContext() instanceof WeightListening) {
                    ((WeightListening) getContext()).onWeightTyping(Integer.parseInt(edtWeight.getText().toString().trim()));
                    dismiss();
                }
            }
        });

        edtWeight.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                tvOk.performClick();
                return false;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.AnimBottomTranslateDelay);
        getDialog().getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
    }

    public static void open(FragmentManager fragmentManager, int oldWeight) {
        WeightDialog weightDialog = new WeightDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("weight", oldWeight);
        weightDialog.setArguments(bundle);
        weightDialog.show(fragmentManager, weightDialog.toString());
    }

    public interface WeightListening {
        void onWeightTyping(int weight);
    }
}
package tools.timestamplogger.ui.dialogs;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import tools.timestamplogger.R;
import tools.timestamplogger.ui.intalize.BaseDialog;

/**
 * Created by nghianv on 5/16/17
 */

public class AgeDialog extends BaseDialog {

    @Override
    protected int getLayout() {
        return R.layout.dialog_age;
    }

    @Override
    protected void afterViews() {
        intiViews();
    }

    private void intiViews() {
        int oldage = getArguments().getInt("age");


        final TextView tvOk = (TextView) view.findViewById(R.id.tvOk);
        final EditText edtAge = (EditText) view.findViewById(R.id.edtAge);

        if (oldage != 0) edtAge.setText(String.valueOf(oldage));

        tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtAge.getText().toString().trim().length() > 0 && edtAge.getText().toString().trim().length() < 3 && Integer.parseInt(edtAge.getText().toString().trim()) > 0 &&
                        getContext() instanceof AgeListening) {
                    ((AgeListening) getContext()).onAgeTyping(Integer.parseInt(edtAge.getText().toString().trim()));
                    dismiss();
                }
            }
        });

        edtAge.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                tvOk.performClick();
                return false;
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        getDialog().getWindow().setWindowAnimations(R.style.AnimBottomTranslateDelay);
        getDialog().getWindow().setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
    }

    public static void open(FragmentManager fragmentManager, int oldAge) {
        AgeDialog ageDialog = new AgeDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("age", oldAge);
        ageDialog.setArguments(bundle);
        ageDialog.show(fragmentManager, ageDialog.toString());
    }

    public interface AgeListening {
        void onAgeTyping(int age);
    }
}
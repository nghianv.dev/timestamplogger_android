package tools.timestamplogger.common.bus;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by nghianv on 26/08/2016
 */
public final class BusProvider {

    private static final EventBus BUS = new EventBus();

    public static EventBus getInstance() {
        return BUS;
    }

    private BusProvider() {
        // No instances.
    }
}

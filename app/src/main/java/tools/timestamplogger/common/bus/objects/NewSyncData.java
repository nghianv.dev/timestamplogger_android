package tools.timestamplogger.common.bus.objects;

import lombok.Data;
import lombok.experimental.Builder;

/**
 * Created by nghianv on 4/12/17
 */
@Data
@Builder
public class NewSyncData {
}

package tools.timestamplogger.common.local_data;

import java.util.List;

import tools.timestamplogger.common.objects.ListOverallEvaluationPendingSync;

/**
 * Created by nghianv on 5/16/17
 */

public class ListOverallEvaluationPendingSyncLocal {

    public static void saveOverallEvaluation(ListOverallEvaluationPendingSync listOverallEvaluationPendingSync) {
        listOverallEvaluationPendingSync.save();
    }

    public static ListOverallEvaluationPendingSync getOverallEvaluation(String userId) {
        List<ListOverallEvaluationPendingSync> listOverallEvaluationPendingSyncs = ListOverallEvaluationPendingSync.findWithQuery(ListOverallEvaluationPendingSync.class,
                "SELECT * FROM overall_evaluation_pendingSync WHERE user_id = ?", userId);
        return (listOverallEvaluationPendingSyncs.size() == 0) ? null : listOverallEvaluationPendingSyncs.get(0);
    }

    public static void deleteOverallEvaluationUser(String userId) {
        ListOverallEvaluationPendingSync.executeQuery("DELETE FROM overall_evaluation_pendingSync WHERE user_id = ?", userId);
    }
}

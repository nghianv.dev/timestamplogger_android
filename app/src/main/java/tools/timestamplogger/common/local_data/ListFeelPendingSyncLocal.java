package tools.timestamplogger.common.local_data;

import java.util.Date;
import java.util.List;

import tools.timestamplogger.common.objects.ListFeelPendingSync;

/**
 * Created by nghianv on 4/21/17
 */

public class ListFeelPendingSyncLocal {
    public static void saveFeel(ListFeelPendingSync listFeelPendingSync) {
        listFeelPendingSync.setTimestamp(new Date().getTime());
        listFeelPendingSync.save();
    }

    public static List<ListFeelPendingSync> getFeels(String userId) {
        return ListFeelPendingSync.findWithQuery(ListFeelPendingSync.class,
                "SELECT * FROM feels_syncs WHERE user_id = ? ORDER BY timestamp ASC",
                userId);
    }

    public static void deleteFeelsSyncUser(String userId) {
        ListFeelPendingSync.executeQuery("DELETE FROM feels_syncs WHERE user_id = ?", userId);
    }
}

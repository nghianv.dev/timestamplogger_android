package tools.timestamplogger.common.local_data;

import java.util.List;

import tools.timestamplogger.common.objects.User;

/**
 * Created by nghianv on 5/16/17
 */

public class UserLocal {

    public static User getUser() {
        List<User> users = User.findWithQuery(User.class, "SELECT * FROM user");
        return users.size() == 0 ? null : users.get(0);
    }

    public static void saveUser(User user) {
        User.executeQuery("DELETE FROM user");
        user.save();
    }

    public static void deleteUser() {
        User.executeQuery("DELETE FROM user");
    }
}

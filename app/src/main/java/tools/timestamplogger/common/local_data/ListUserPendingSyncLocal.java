package tools.timestamplogger.common.local_data;

import java.util.List;

import tools.timestamplogger.common.objects.ListUserPendingSync;

/**
 * Created by nghianv on 5/16/17
 */

public class ListUserPendingSyncLocal {

    public static void addUserPendingSync(ListUserPendingSync listUserPendingSync) {
        listUserPendingSync.save();
    }

    public static List<ListUserPendingSync> getUserPendingSync() {
        return ListUserPendingSync.findWithQuery(ListUserPendingSync.class,
                "SELECT * FROM user_syncs WHERE ready = 1");
    }

    public static void updateReadyStatus(String userId, int ready) {
        ListUserPendingSync.executeQuery("UPDATE user_syncs SET ready = ? WHERE user_id = ?", String.valueOf(ready), userId);
    }

    public static void deleteUserPendingSync(String userId) {
        ListUserPendingSync.executeQuery("DELETE FROM user_syncs WHERE user_id = ?", userId);
    }
}

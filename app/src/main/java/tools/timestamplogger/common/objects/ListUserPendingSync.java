package tools.timestamplogger.common.objects;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Builder;

/**
 * Created by nghianv on 4/21/17
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor(suppressConstructorProperties = true)
@Builder
@Table(name = "user_syncs")
public class ListUserPendingSync extends SugarRecord {
    @Column(name = "user_id")
    private String userId;
    @Column(name = "participant")
    private String participant;
    @Column(name = "gender")
    private String gender;
    @Column(name = "age")
    private int age;
    @Column(name = "weight")
    private int weight;
    @Column(name = "height")
    private int height;
    @Column(name = "bus_route")
    private int busRoute;
    @Column(name = "position")
    private String position;
    @Column(name = "ready")
    private int ready;

    public ListUserPendingSync() {

    }
}

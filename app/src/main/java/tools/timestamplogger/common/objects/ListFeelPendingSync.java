package tools.timestamplogger.common.objects;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Builder;

/**
 * Created by nghianv on 5/16/17
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor(suppressConstructorProperties = true)
@Builder
@Table(name = "feels_syncs")
public class ListFeelPendingSync extends SugarRecord {
    @Column(name = "user_id")
    private String userId;
    @Column(name = "feel")
    private int feel;
    @Column(name = "lat")
    private String lat;
    @Column(name = "lng")
    private String lng;
    @Column(name = "time")
    private String time;
    @Column(name = "timestamp")
    private long timestamp;


    public ListFeelPendingSync() {

    }
}

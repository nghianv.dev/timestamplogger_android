package tools.timestamplogger.common.objects;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Builder;

/**
 * Created by nghianv on 5/16/17
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor(suppressConstructorProperties = true)
@Builder
@Table(name = "overall_evaluation_pendingSync")
public class ListOverallEvaluationPendingSync extends SugarRecord {
    @Column(name = "user_id")
    private String userId;
    @Column(name = "ride_comfort")
    private int rideComfort;
    @Column(name = "road_ondition")
    private int roadCondition;
    @Column(name = "driving_condition")
    private int drivingCondition;
    @Column(name = "vehicle_condition")
    private int vehicleCondition;

    public ListOverallEvaluationPendingSync() {

    }
}

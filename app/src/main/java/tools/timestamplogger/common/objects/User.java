package tools.timestamplogger.common.objects;

import com.orm.SugarRecord;
import com.orm.dsl.Column;
import com.orm.dsl.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Builder;

/**
 * Created by nghianv on 5/16/17
 */
@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor(suppressConstructorProperties = true)
@Builder
@Table(name = "user")
public class User extends SugarRecord {
    @Column(name = "_id")
    private String _id;
    @Column(name = "participant")
    private String participant;
    // 0-woman | 1-man
    @Column(name = "gender")
    private String gender;
    @Column(name = "age")
    private int age;
    @Column(name = "weight")
    private int weight;
    @Column(name = "height")
    private int height;
    @Column(name = "bus_router")
    private int busRoute;
    @Column(name = "position")
    private String position;
    @Column(name = "is_start")
    private int isStart;

    public User() {

    }
}

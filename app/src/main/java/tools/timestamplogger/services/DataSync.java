package tools.timestamplogger.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import android.util.Log;

import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import tools.timestamplogger.common.bus.BusProvider;
import tools.timestamplogger.common.bus.objects.NewSyncData;
import tools.timestamplogger.common.local_data.ListFeelPendingSyncLocal;
import tools.timestamplogger.common.local_data.ListOverallEvaluationPendingSyncLocal;
import tools.timestamplogger.common.local_data.ListUserPendingSyncLocal;
import tools.timestamplogger.common.objects.ListFeelPendingSync;
import tools.timestamplogger.common.objects.ListOverallEvaluationPendingSync;
import tools.timestamplogger.common.objects.ListUserPendingSync;
import tools.timestamplogger.network.apis.WebApi;

public class DataSync extends Service {

    private static DataSync dataSync;
    private boolean hasNetwork;

    public static boolean isRunning() {
        return dataSync != null;
    }

    private BroadcastReceiver networkStateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo ni = manager.getActiveNetworkInfo();
            if (ni != null && ni.isConnected()) {
                startOnlineNetwork();
            } else {
                startOfflineNetwork();
            }
        }
    };

    public DataSync() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        dataSync = this;
        BusProvider.getInstance().register(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        registerReceiver(networkStateReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        dataSync = null;
        super.onDestroy();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void onNewDataSync(NewSyncData newSyncData) {
        if (hasNetwork) {
            checkAndSyncData();
        }
    }

    private void startOnlineNetwork() {
        hasNetwork = true;
        checkAndSyncData();
    }

    private void startOfflineNetwork() {
        hasNetwork = false;
    }

    private void checkAndSyncData() {
        List<ListUserPendingSync> listUserPendingSyncs = ListUserPendingSyncLocal.getUserPendingSync();
        if (listUserPendingSyncs.size() > 0) {
            for (final ListUserPendingSync listUserPendingSync : listUserPendingSyncs) {
                ArrayList<ListFeelPendingSync> listFeelPendingSyncs = (ArrayList<ListFeelPendingSync>) ListFeelPendingSyncLocal.getFeels(listUserPendingSync.getUserId());
                ListOverallEvaluationPendingSync overallEvaluation = ListOverallEvaluationPendingSyncLocal.getOverallEvaluation(listUserPendingSync.getUserId());

                WebApi.saveResult(listUserPendingSync.getUserId(), listUserPendingSync.getBusRoute(),
                        listUserPendingSync.getPosition(),
                        listUserPendingSync.getParticipant(), listUserPendingSync.getGender(),
                        listUserPendingSync.getAge(), listUserPendingSync.getHeight(),
                        listUserPendingSync.getWeight(), overallEvaluation.getRideComfort(),
                        overallEvaluation.getRoadCondition(), overallEvaluation.getDrivingCondition(),
                        overallEvaluation.getVehicleCondition(), listFeelPendingSyncs, "Kg", "cm", new WebApi.NetWorkListening() {
                            @Override
                            public void onSuccess(String message) {
                                Log.d("DataSync", "Gui du lieu thanh cong");
                                ListFeelPendingSyncLocal.deleteFeelsSyncUser(listUserPendingSync.getUserId());
                                ListOverallEvaluationPendingSyncLocal.deleteOverallEvaluationUser(listUserPendingSync.getUserId());
                                listUserPendingSync.delete();
                            }

                            @Override
                            public void onErrorRequest(String error) {
                                Log.d("DataSync", "gui du lieu that bai: " + error);
                            }
                        });
            }
        }
    }
}

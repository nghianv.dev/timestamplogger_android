package tools.timestamplogger.network.apis;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import tools.timestamplogger.common.objects.ListFeelPendingSync;
import tools.timestamplogger.network.ResponseData;
import tools.timestamplogger.network.interfaces.instance.WebServer;

/**
 * Created by nghianv on 4/3/17
 */

public class WebApi {

    public static void saveResult(String userId, int busRoute, String position, String participant,
                                  String gender, int age, int height, int weight,
                                  int ride, int road, int driving, int vehicle,
                                  List<ListFeelPendingSync> feelsDetail, String unitWeight, String unitHeight,
                                  final NetWorkListening netWorkListening) {

        String feelsDetailStr = new Gson().toJson(feelsDetail);

        WebServer.getInstance().getApis().saveResult(userId, busRoute, position, participant, gender, age, height,
                weight, ride, road, driving, vehicle, feelsDetailStr, unitHeight, unitWeight).enqueue(new Callback<ResponseData>() {

            @Override
            public void onResponse(@NonNull Call<ResponseData> call, @NonNull Response<ResponseData> response) {
                Log.d("WebAPI", response.toString());
                if (response.code() == 200) {
                    if (!response.body().isError()) {
                        netWorkListening.onSuccess(response.body().getMessage());
                    } else {
                        netWorkListening.onErrorRequest(response.body().getMessage());
                    }
                } else {
                    netWorkListening.onErrorRequest(response.message());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseData> call, @NonNull Throwable t) {
                netWorkListening.onErrorRequest(t.getMessage());
            }
        });
    }

    public interface NetWorkListening {
        void onSuccess(String message);

        void onErrorRequest(String error);
    }
}

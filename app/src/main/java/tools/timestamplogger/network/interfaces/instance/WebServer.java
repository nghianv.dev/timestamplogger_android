package tools.timestamplogger.network.interfaces.instance;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import tools.timestamplogger.network.interfaces.WebInterface;

/**
 * Created by nghianv on 4/3/17
 */

public class WebServer {
    private static final WebServer ourInstance = new WebServer();
    private WebInterface webInterface;

    public static WebServer getInstance() {
        return ourInstance;
    }

    public WebInterface getApis() {
        return webInterface;
    }

    static final class CookieInterceptor implements Interceptor {
        private volatile String cookie;

        public void setSessionCookie(String cookie) {
            this.cookie = cookie;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            if (this.cookie != null) {
                request = request.newBuilder()
                        .header("Cookie", this.cookie)
                        .build();
            }
            return chain.proceed(request);
        }
    }

    private WebServer() {
        CookieInterceptor cookieInterceptor = new CookieInterceptor();
//        cookieInterceptor.setSessionCookie("__test=438955e42fd1134a2048d2c1966c12bf; _ga=GA1.2.540111008.1494951248; _gid=GA1.2.1318644570.1494952594");
//        cookieInterceptor.setSessionCookie("_ga=GA1.2.540111008.1494951248; __test=438955e42fd1134a2048d2c1966c12bf");

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(cookieInterceptor)
                .build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();

        webInterface = retrofit.create(WebInterface.class);
    }
}

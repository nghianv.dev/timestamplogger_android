package tools.timestamplogger.network.interfaces;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import tools.timestamplogger.network.ResponseData;

/**
 * Created by nghianv on 4/3/17
 */

public interface WebInterface {

    @FormUrlEncoded
    @POST("/")
    Call<ResponseData> saveResult(
            @Field("userId") String userId,
            @Field("busRoute") int busRoute,
            @Field("position") String position,
            @Field("participant") String participant,
            @Field("gender") String gender,
            @Field("age") int age,
            @Field("height") int height,
            @Field("weight") int weight,
            @Field("ride") int ride,
            @Field("road") int road,
            @Field("driving") int driving,
            @Field("vehicle") int vehicle,
            @Field("feelsDetail") String feelsDetail,
            @Field("unitHeight") String unitHeight,
            @Field("unitWeight") String unitWeight
    );
}

package tools.timestamplogger.network;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.Builder;

/**
 * Created by nghianv on 4/3/17
 */
@Data
@Builder
@AllArgsConstructor(suppressConstructorProperties = true)
public class ResponseData {
    private boolean error;
    private String message;
}

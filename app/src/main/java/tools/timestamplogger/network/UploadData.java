package tools.timestamplogger.network;

import java.util.List;

import lombok.experimental.Builder;
import tools.timestamplogger.common.objects.ListFeelPendingSync;

/**
 * Created by nghianv on 5/17/17
 */
@Builder
public class UploadData {
    private String userId;
    private int busRoute;
    private String participant;
    private String gender;
    private int age;
    private int height;
    private int weight;
    private int ride;
    private int road;
    private int driving;
    private int vehicle;
    private List<ListFeelPendingSync> feelsDetail;
    private String unitHeight;
    private String unitWeight;
}

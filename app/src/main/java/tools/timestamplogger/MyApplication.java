package tools.timestamplogger;

import android.annotation.SuppressLint;
import android.app.Application;

import com.orm.SugarContext;

import org.androidannotations.annotations.EApplication;

/**
 * Created by nghianv on 5/16/17
 */
@SuppressLint("Registered")
@EApplication
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        SugarContext.terminate();
    }
}
